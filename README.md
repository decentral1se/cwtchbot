# Cwtch Bot Framework

A specialized Cwtch Bot framework in Go that provides a more lightweight and tailored approach to building chat bots for Cwtch. 

For an introduction to building chatbots with the CwtchBot framework check out [the building an echobot tutorial](https://docs.cwtch.im/developing/building-a-cwtch-app/building-an-echobot).

If you'd like to get involved please open an issue, or submit a pull request :) 